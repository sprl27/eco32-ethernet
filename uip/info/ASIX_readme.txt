    This is the uIP web server application for ASIX AX88796B ethernet 
    controller + 8051 Demo board (Rev1.1 Ethernet only)
REQUIREMENTS.

 * Keil Elektronik GmbH/ Keil Software Inc. C51 v7.00 compiler. 
   This version is known to compile with the uVision2 v2.xx toolchain.

 * AX88796B + 8051 demo board
 
COPYRIGHT

    This code is modified from the AX88796 + 8051 uIP port Rev 1.1 
    by changing the Memory Model of the project file (Ethernet.Uv2) 
    target options from "Large: variables in XDATA" to 
    "Small: variables in DATA".

    This code is ported from the Keil C51/ 8051 port of Adam Dunkels' 
    uIP v0.9 TCP/IP stack by Murray R. Van Luyn.(Ethernet & SLIP)

  	For more information about uIP 
  		http://dunkels.com/adam/uip/
  	
	Keil C51/ 8051 port by Murray R. Van Luyn.
		http://www.iinet.net.au/~vanluynm

	They are released free of charge for any purpose, including both 
	commercial and non-commercial.
	
CHANGES
Rev1.1
* Add etherdev_chkmedia function (in case of speed change)
* Change Copy right header
Rev1.0
* The code is compiled in large mode because the AX88796B base address
  is map to xram 0x8300 address
* Timer function is separated from etherdev.c and timer.c added
* Etherdev.c is repladced with AX88796B driver

DISCLAIMER.

    THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS
    OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  

Contact

ASIX Electronic Corporation website
http://www.asix.com.tw
E-mail: sales@asix.com.tw 
