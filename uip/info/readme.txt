
This software is a port of "Keil C51 compiler/ 8051 microcontroller port of
Adam Dunkels' uIP TCP/IP stack v0.9" for the ECO32 CPU.

You can find information about the ECO32 CPU here:
	http://opencores.org/project,eco32

Please read the "readme_original.txt" file for information about the original
authors.

Contact:
	Simeon Perlov
	simeon@perlov.de
