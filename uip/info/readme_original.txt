          ---------------------------------------------------------- 
         |    TITLE:  Keil C51 v7.00 port of Adam Dunkels' uIP v0.9 |
         | REVISION:  VER 0.0                                       |
         | REV.DATE:  02-02-05                                      |
         |  ARCHIVE:                                                |
         |   AUTHOR:  Copyright (c) 2005, Murray R. Van Luyn.       |
          ----------------------------------------------------------         



INTRODUCTION.

    This is the Keil C51 compiler/ 8051 microcontroller port of Adam Dunkels'
    uIP TCP/IP stack v0.9.

    Included is the uIP web server application, as well as drivers for both 
    serial port SLIP and network card ethernet communication. 



REQUIREMENTS.

 * Keil Elektronik GmbH/ Keil Software Inc. C51 v7.00 compiler. 
   This version is known to compile with the uVision2 v2.xx toolchain.

 * An 8051/ 8052 class microcontroller with 256 bytes of internal ram
   (data and idata), up to 18K bytes of FLASH or other program ROM (code),
   and additionally, 1K bytes of onboard or outboard external ram (xram).

 * For ethernet communication: A 16 bit ISA network interface card with a
   Realtek Semiconductor Co., Ltd. RTL8019AS controller chip.



COPYRIGHT.

    Adam Dunkels retains ownership and copyright on all files ported from the 
    original uIP v0.9 TCP/IP stack release. Refer to the original release
    notes, and to the ported file's headers for copyright details and usage
    conditions.
                         http://www.sics.se/~adam/uip/

    Additional files contained in this port, that are not derived from the 
    original uIP v0.9 TCP/IP stack release, are Copyright (c) 2005,
    Murray R. Van Luyn, Riverton, Western Australia. They are released free
    of charge for any purpose, including both commercial and non-commercial.



DISCLAIMER.

    THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS
    OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
    GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  



DISTRIBUTION.

    I hope to have the most up-to-date and correct version of this port
    available for distribution from my homepage at all times. In preference
    to multiple, possibly outdated and un-manageable points of distribution,
    I would welcome links to this one point of origin. As the port's filename
    may change with each revision, please link to the root page from which the
    download may be located.

                      http://www.iinet.net.au/~vanluynm


CREDITS.

    This Keil C51/ 8051 port of uIP v0.9 is very substantially the work of the 
    original distribution's author, and is in part an extension of the work of
    previous uIP contributors.

        Original uIP v0.9 TCP/IP stack by Adam Dunkels.
        8051WEB Micro-C port of uIP v0.6 to 8051 by Fajun Chen.
        GCC/ Imagecraft port of uIP v0.9 to AVR by Louis Beaudoin.



QUERIES, COMMENTS, SUGGESTIONS, PORT CONTRIBUTIONS, SUCCESS STORIES ETC.

    Correspondence relating to the Keil C51/ 8051 port of Adam Dunkels' uIP 
    TCP/IP stack v0.9 may be directed to my personal e-mail account.

                            Murray R. Van Luyn.
                           vanluynm@iinet.net.au

    Please don't be offended if I do not answer right away. It probably just
    means that I'm busy with study.
