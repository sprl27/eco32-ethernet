
#include "interrupt.h"
#include "timer.h"

#include "port/tinyprintf/tinyprintf.h"

#define PSW_DEV(_psw) ((_psw >> 16) & 0x1F)

void print_error(unsigned int psw, unsigned int source,
		 unsigned int instr, unsigned int *stack)
{
	unsigned int i, j, reg, data;
  printf("Stack Pointer: 0x%X\n", stack);
	printf("Interrupt: PSW: 0x%X, Device: #%d\n", psw, PSW_DEV(psw));
  printf("Location: 0x%X, Instruction: 0x%X\n", source, instr);
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 4; j++) {
			reg = 8 * j + i;
			if (reg == 0) {
				data = 0;
			} else if (reg == 29) {
				data = (unsigned int)stack;
			} else if (reg > 29) {
				data = stack[reg - 1];
			} else {
				data = stack[reg];
			}
			printf("$%-2d  %08X     ", reg, data);
		}
		printf("\n");
	}

  /* Stop program execution */
  while (1) ;
}

void handler_interrupt(unsigned int psw, unsigned int source,
		       unsigned int instr, unsigned int *stack)
{
	if (PSW_DEV(psw) == 14) {
		timer0_isr();
	} else {
		print_error(psw, source, instr, stack);
	}
}

void handler_usermiss(unsigned int psw, unsigned int source,
		      unsigned int instr, unsigned int *stack)
{
	print_error(psw, source, instr, stack);
}
