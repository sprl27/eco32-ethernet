/*
 * helper.h
 */


#ifndef _HELPER_H_
#define _HELPER_H_


void enable (void);
void disable (void);
void orMask (unsigned int mask);


#endif /* _HELPER_H_ */
