/*
 * iolib.h -- I/O library
 */


#ifndef _IOLIB_H_
#define _IOLIB_H_


int strlen (char *str);
int strcmp (char *s1, char *s2);
void strcpy (char *dst, char *src);
void memcpy (void *dst, void *src, unsigned int cnt);
void memset (void *dst, unsigned char c, unsigned int cnt);
char getchar (void);
void putchar (char c);
void putString (char *s);
void getLine (char *prompt, char *line, int max);


#endif /* _IOLIB_H_ */
