;
; helper.s
;

	.export	enable
	.export	disable
	.export	orMask

enable:
	mvfs	$8,0
	or	$8,$8,1 << 23
	mvts	$8,0
	jr	$31

disable:
	mvfs	$8,0
	and	$8,$8,~(1 << 23)
	mvts	$8,0
	jr	$31

orMask:
	mvfs	$8,0
	and	$4,$4,0x0000FFFF	; use lower 16 bits only
	or	$8,$8,$4
	mvts	$8,0
	jr	$31
