/*
 * biolib.h -- basic I/O library
 */


#ifndef _BIOLIB_H_
#define _BIOLIB_H_


#define DEV_SERIAL (0xF0300000)


char getc (void);
void putc (char c);


#endif /* _BIOLIB_H_ */
