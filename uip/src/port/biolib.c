/*
 * biolib.c -- basic I/O library
 */


#include "biolib.h"


char
getc (void)
{
  volatile unsigned int *base;
  char c;

  base = (unsigned int *) DEV_SERIAL;
  while ((*(base + 0) & 1) == 0);
  c = *(base + 1);
  return c;
}


void
putc (char c)
{
  volatile unsigned int *base;

  base = (unsigned int *) DEV_SERIAL;
  while ((*(base + 2) & 1) == 0);
  *(base + 3) = c;
}
