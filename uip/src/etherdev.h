/*        ---------------------------------------------------------- 
         |    TITLE:  Keil C51 v7.00 port of Adam Dunkels' uIP v0.9 |
         | REVISION:  VER 0.0                                       |
         | REV.DATE:  30-01-05                                      |
         |  ARCHIVE:                                                |
         |   AUTHOR:  Murray R. Van Luyn, 2005.                     |
          ----------------------------------------------------------         */

/*   --------------------------------------------------------------------- 

    |  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS  | 
    |  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED  | 
    |  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE | 
    |  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY    | 
    |  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL | 
    |  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  | 
    |  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS      | 
    |  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,       | 
    |  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING          | 
    |  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS | 
    |  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.       | 
     ---------------------------------------------------------------------   */

/*=============================================================================
 * Module Name:etherdev.h
 * Purpose:
 * Author:
 * Date:
 * Notes:
 * $Log$                                                                                                                        
*=============================================================================
*/
#ifndef _ETHERDEV_H_
#define _ETHERDEV_H_

typedef unsigned char UCHAR, *PUCHAR;
typedef unsigned int UINT, *PUINT;
typedef unsigned short USHORT, *PUSHORT;
typedef unsigned long ULONG, *PULONG;

/*---------------------------------------------------------------
	MAC register
---------------------------------------------------------------*/
#define NIC_COMMAND			0x0	// (CR)
#define NIC_PAGE_START		0x1	// (PSTART)             MSB, write-only
#define NIC_PHYS_ADDR		0x1	// (PAR0)               page 1
#define NIC_PAGE_STOP		0x2	// (PSTOP)              MSB, write-only
#define NIC_BOUNDARY		0x3	// (BNRY)               MSB
#define NIC_XMIT_START		0x4	// (TPSR)               MSB, write-only
#define NIC_XMIT_STATUS		0x4	// (TSR)                read-only
#define NIC_XMIT_COUNT_LSB	0x5	// (TBCR0)              write-only
#define NIC_XMIT_COUNT_MSB	0x6	// (TBCR1)              write-only
#define NIC_INTR_STATUS		0x7	// (ISR)
#define NIC_CURRENT			0x7	// (CURR)               page 1
#define NIC_MC_ADDR			0x8	// (MAR0)               page 1
#define NIC_CRDA_LSB		0x8	// (CRDA0)
#define NIC_RMT_ADDR_LSB	0x8	// (RSAR0)
#define NIC_CRDA_MSB		0x9	// (CRDA1)
#define NIC_RMT_ADDR_MSB	0x9	// (RSAR1)
#define NIC_RMT_COUNT_LSB	0xa	// (RBCR0)              write-only
#define NIC_RMT_COUNT_MSB	0xb	// (RBCR1)              write-only
#define NIC_RCV_CONFIG		0xc	// (RCR)                write-only
#define NIC_RCV_STATUS		0xc	// (RSR)                read-only
#define NIC_XMIT_CONFIG		0xd	// (TCR)                write-only
#define NIC_FAE_ERR_CNTR	0xd	// (CNTR0)              read-only
#define NIC_DATA_CONFIG		0xe	// (DCR)                write-only
#define NIC_CRC_ERR_CNTR	0xe	// (CNTR1)              read-only
#define NIC_INTR_MASK		0xf	// (IMR)                write-only
#define NIC_MISSED_CNTR		0xf	// (CNTR2)              read-only
#define NIC_RACK_NIC		0x10	// Byte to read or write
#define NIC_RESET			0x1f	// (RESET)

// Constants for the NIC_COMMAND register.
#define CR_STOP			(UCHAR)0x01	// reset the card
#define CR_START		(UCHAR)0x02	// start the card
#define CR_XMIT			(UCHAR)0x04	// begin transmission
#define CR_NO_DMA		(UCHAR)0x20	// stop remote DMA
#define CR_PAGE0		(UCHAR)0x00	// select page 0
#define CR_PAGE1		(UCHAR)0x40	// select page 1
#define CR_DMA_WRITE	(UCHAR)0x10	// Write
#define CR_DMA_READ		(UCHAR)0x08	// Read

// Constants for the NIC_XMIT_STATUS register.
#define TSR_XMIT_OK		(UCHAR)0x01	// transmit with no errors
#define TSR_COLLISION	(UCHAR)0x04	// collided at least once
#define TSR_ABORTED		(UCHAR)0x08	// too many collisions

// Constants for the NIC_INTR_STATUS register.
#define ISR_RCV			(UCHAR)0x01	// packet received with no errors
#define ISR_XMIT		(UCHAR)0x02	// packet transmitted with no errors
#define ISR_RCV_ERR		(UCHAR)0x04	// error on packet reception
#define ISR_XMIT_ERR	(UCHAR)0x08	// error on packet transmission
#define ISR_OVERFLOW	(UCHAR)0x10	// receive buffer overflow
#define ISR_COUNTER		(UCHAR)0x20	// MSB set on tally counter
#define ISR_DMA_DONE	(UCHAR)0x40	// RDC
#define ISR_RESET		(UCHAR)0x80	// (not an interrupt) card is reset

// Constants for the NIC_RCV_CONFIG register.
#define RCR_BROADCAST	(UCHAR)0x04	// receive broadcast packets
#define RCR_MULTICAST	(UCHAR)0x08	// receive multicast packets
#define RCR_ALL_PHYS	(UCHAR)0x10	// receive ALL directed packets
#define RCR_MONITOR		(UCHAR)0x20	// don't collect packets
#define RCR_INT_ACT		(UCHAR)0x40

// Constants for the NIC_RCV_STATUS register.
#define RSR_PACKET_OK	(UCHAR)0x01	// packet received with no errors
#define RSR_CRC_ERROR	(UCHAR)0x02	// packet received with CRC error
#define RSR_MULTICAST	(UCHAR)0x20	// packet received was multicast
#define RSR_DISABLED	(UCHAR)0x40	// received is disabled

// Constants for the NIC_XMIT_CONFIG register.
#define TCR_LOOPBACK	(UCHAR)0x02	// loopback (set when NIC is stopped)

// Constants for the NIC_DATA_CONFIG register.
#define DCR_BYTE_WIDE		(UCHAR)0x00	// byte-wide DMA transfers
#define DCR_WORD_WIDE		(UCHAR)0x01	// word-wide DMA transfers

// Constants for the NIC_INTR_MASK register.
// Configure which ISR settings actually cause interrupts.
#define IMR_RCV			(UCHAR)0x01	// packet received with no errors
#define IMR_XMIT		(UCHAR)0x02	// packet transmitted with no errors
#define IMR_RCV_ERR		(UCHAR)0x04	// error on packet reception
#define IMR_XMIT_ERR	(UCHAR)0x08	// error on packet transmission
#define IMR_OVERFLOW	(UCHAR)0x10	// receive buffer overflow
#define IMR_COUNTER		(UCHAR)0x20	// MSB set on tally counter

#define INT_MASK	(IMR_RCV | IMR_XMIT | IMR_XMIT_ERR | IMR_OVERFLOW)
#define RCV_CONFIG	(RCR_BROADCAST | RCR_INT_ACT)

// xmit space
#define XMIT_NUM_BUF	12
#define XMIT_START_PAGE	0x40

// receive space
#define RCV_START_PAGE	0x4c
#define RCV_STOP_PAGE	0x80

// Ethernet
#define MAX_ETHERNET_LEN		UIP_BUFSIZE
#define MIN_ETHERNET_LEN		60
#define ETH_ADDR_LEN			6

/* EXPORTED SUBPROGRAM SPECIFICATIONS */
void etherdev_init(void);
void etherdev_send(void);
unsigned int etherdev_poll(void);
void etherdev_chkmedia(void);

#endif

/* End of etherdev.h */
