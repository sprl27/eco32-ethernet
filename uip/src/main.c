/*        ---------------------------------------------------------- 
         |    TITLE:  Keil C51 v7.00 port of Adam Dunkels' uIP v0.9 |
         | REVISION:  VER 0.1                                       |
         | REV.DATE:  30-01-05                                      |
         |  ARCHIVE:                                                |
         |   AUTHOR:  Murray R. Van Luyn, 2005.                     |
          ----------------------------------------------------------         */

/*   --------------------------------------------------------------------- 
    |  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS  | 
    |  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED  | 
    |  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE | 
    |  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY    | 
    |  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL | 
    |  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  | 
    |  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS      | 
    |  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,       | 
    |  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING          | 
    |  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS | 
    |  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.       | 
     ---------------------------------------------------------------------   */

#include "main.h"
#include "timer.h"

#include "biolib.h"
#include "port/tinyprintf/tinyprintf.h"

void init_putc(void *p, char c)
{
	if (c == '\n') {
		putc('\r');
	}
	putc(c);
}

/*   --------------------------------------------------------------------- 
    |                                                                     |
    |                                  main()                             |
    |                                                                     |
    | Simple Web Server test application.                                 |
    |                                                                     |
    |                                                                     |
     ---------------------------------------------------------------------   */
void main(void)
{
	u8_t  i, arptimer;

	init_printf(NULL, init_putc);
	printf("Starting uIP.\n");

	timer0_init();
	/* Initialise the uIP TCP/IP stack. */
	uip_init();

	/* Initialise the app. */
	httpd_init();

	/* Initialise the device driver. */
	etherdev_init();

	/* Initialise the ARP cache. */
	uip_arp_init();

	arptimer = 0;

	printf("Initialization successful.\n");

	/* to check ethernet had link or not and check connection type. */
	etherdev_chkmedia();
	printf("Ethernet link found.\n");

	while (1) {
		uip_len = etherdev_poll();

		if (uip_len == 0) {
			/* to check ethernet had link or not and check connection type. */
			etherdev_chkmedia();

			for (i = 0; i < UIP_CONNS; i++) {
				uip_periodic(i);
				/* If the above function invocation resulted in data that
				   should be sent out on the network, the global variable
				   uip_len is set to a value > 0. */
				if (uip_len > 0) {
					uip_arp_out();
					etherdev_send();
				}
			}

#if UIP_UDP
			for (i = 0; i < UIP_UDP_CONNS; i++) {
				uip_udp_periodic(i);
				/* If the above function invocation resulted in data that
				   should be sent out on the network, the global variable
				   uip_len is set to a value > 0. */
				if (uip_len > 0) {
					uip_arp_out();
					etherdev_send();
				}
			}
#endif				/* UIP_UDP */

			/* Call the ARP timer function every 10 seconds. */
			if (++arptimer == 20) {
				uip_arp_timer();
				arptimer = 0;
			}
		} else {	/* (uip_len != 0) Process incoming */

			if (BUF->type == htons(UIP_ETHTYPE_IP)) {
				uip_arp_ipin();
				uip_input();
				/* If the above function invocation resulted in data that
				   should be sent out on the network, the global variable
				   uip_len is set to a value > 0. */
				if (uip_len > 0) {
					uip_arp_out();
					etherdev_send();
				}
			} else if (BUF->type == htons(UIP_ETHTYPE_ARP)) {
				uip_arp_arpin();
				/* If the above function invocation resulted in data that
				   should be sent out on the network, the global variable
				   uip_len is set to a value > 0. */
				if (uip_len > 0) {
					etherdev_send();
				}
			}
		}
	}

	return;
}
