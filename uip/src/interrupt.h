
#ifndef INTERRUPT_H
#define INTERRUPT_H

void handler_interrupt(unsigned int psw, unsigned int source,
		       unsigned int instr, unsigned int *stack);
void handler_usermiss(unsigned int psw, unsigned int source,
		      unsigned int instr, unsigned int *stack);

#endif
