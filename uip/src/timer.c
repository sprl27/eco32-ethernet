/*        ---------------------------------------------------------- 
         |    TITLE:  Keil C51 v7.00 port of Adam Dunkels' uIP v0.9 |
         | REVISION:  VER 0.0                                       |
         | REV.DATE:  30-01-05                                      |
         |  ARCHIVE:                                                |
         |   AUTHOR:  Murray R. Van Luyn, 2005.                     |
          ----------------------------------------------------------         */

/*   --------------------------------------------------------------------- 

    |  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS  | 
    |  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED  | 
    |  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE | 
    |  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY    | 
    |  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL | 
    |  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  | 
    |  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS      | 
    |  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,       | 
    |  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING          | 
    |  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS | 
    |  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.       | 
     ---------------------------------------------------------------------   */

#include "port/helper.h"

#define TIMER_BASE ((unsigned int *) 0xF0000000)

#define ETH_CPU_XTAL        (50000000)	// crystal freq in Hz

// timing parameters
#define ETH_T0_INT_RATE     (24)	// Timer 0 intrupt rate (Hz)
#define ETH_T0_DIV          (ETH_CPU_XTAL / ETH_T0_INT_RATE)

static unsigned int tick_count = 0;

/*   --------------------------------------------------------------------- 
    |                                                                     |
    |                           etherdev_timer0_isr()                     |
    |                                                                     |
    | This function is invoked each 1/24th of a second and updates a      |
    | 1/24th of a second tick counter.                                    |
    |                                                                     |
     ---------------------------------------------------------------------   */

void timer0_isr(void)
{
	// Reload timer/ counter 0 for 24Hz periodic interrupt.
	// reset IRQ signal
	*TIMER_BASE = 2;
	// Increment 24ths of a second counter.
	tick_count++;

	return;
}

unsigned int timer0_tick(void)
{
	return tick_count;
}

void timer0_reset(void)
{
	tick_count = 0;
}

/*   --------------------------------------------------------------------- 
    |                                                                     |
    |                                  _timer_init                        |
    |                                                                     |
    |  Returns: 1 on success, 0 on failure.                               |
    |                                                                     |
     ---------------------------------------------------------------------   */
int timer0_init(void)
{
	// Initialize Timer 0 to generate a periodic 24Hz interrupt.

	// Enable global interrupt
	enable();

	// Initialize timer
	*(TIMER_BASE + 1) = ETH_T0_DIV;
	*TIMER_BASE = 2;
	orMask(1 << 14);

	return 1;
}
