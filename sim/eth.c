/*
 * eth.c -- ethernet
 */


#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "console.h"
#include "eth.h"

static Bool debug = false;

Word ethRead(Word addr) {
  if (debug) {
    cPrintf("\n**** ETH READ from 0x%08X", addr);
  }
  return 0;
}

void ethWrite(Word addr, Word data) {
  if (debug) {
    cPrintf("\n**** ETH WRITE to 0x%08X, data = 0x%08X ****\n",
            addr, data);
  }
  return;
}

void ethReset(void) {
  return;
}

void ethInit(void) {
  ethReset();
  return;
}

void ethExit(void) {
  return;
}
