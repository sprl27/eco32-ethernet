/*
 * eth.h -- ethernet
 */


#ifndef _ETH_H_
#define _ETH_H_

Word ethRead(Word addr);
void ethWrite(Word addr, Word data);

void ethReset(void);
void ethInit(void);
void ethExit(void);

#endif /* _ETH_H_ */
