/*
 * types.h -- additional types
 */


#ifndef _TYPES_H_
#define _TYPES_H_


typedef int Bool;

#define FALSE	0
#define TRUE	1

typedef unsigned int size_t;


#endif /* _TYPES_H_ */
