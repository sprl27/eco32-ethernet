;
; start.s -- startup code
;

;***************************************************************

	.set	dmapaddr,0xC0000000	; base of directly mapped addresses
	.set	TLB_INDEX,1		; reg # of TLB Index
	.set	TLB_ENTRY_HI,2		; reg # of TLB EntryHi
	.set	TLB_ENTRY_LO,3		; reg # of TLB EntryLo
	.set	TLB_ENTRIES,32		; number of TLB entries

	.import	main
	.import handler_interrupt
	.import handler_usermiss

	.import	_ecode
	.import	_edata
	.import	_ebss

	.export	_bcode
	.export	_bdata
	.export	_bbss

;***************************************************************

	.code
_bcode:

	.data
_bdata:

	.bss
_bbss:

;***************************************************************

	.code
	.align	4

start:
	j start1
ex_isr:
	j isr
ex_miss:
	j miss

;***************************************************************

	.code
	.align	4

isr:
	sub	$29,$29,124		; save registers
	stw	$1,$29,4
	stw	$2,$29,8
	stw	$3,$29,12
	stw	$4,$29,16
	stw	$5,$29,20
	stw	$6,$29,24
	stw	$7,$29,28
	stw	$8,$29,32
	stw	$9,$29,36
	stw	$10,$29,40
	stw	$11,$29,44
	stw	$12,$29,48
	stw	$13,$29,52
	stw	$14,$29,56
	stw	$15,$29,60
	stw	$16,$29,64
	stw	$17,$29,68
	stw	$18,$29,72
	stw	$19,$29,76
	stw	$20,$29,80
	stw	$21,$29,84
	stw	$22,$29,88
	stw	$23,$29,92
	stw	$24,$29,96
	stw	$25,$29,100
	stw	$26,$29,104
	stw	$27,$29,108
	stw	$28,$29,112
	stw	$30,$29,116
	stw	$31,$29,120

	sub	$29,$29,100
	mvfs	$4,0
	add	$5,$30,$0
	ldw	$6,$30,0
	add	$7,$29,100
	jal	handler_interrupt
	add	$29,$29,100

	ldw	$1,$29,4		; restore registers
	ldw	$2,$29,8
	ldw	$3,$29,12
	ldw	$4,$29,16
	ldw	$5,$29,20
	ldw	$6,$29,24
	ldw	$7,$29,28
	ldw	$8,$29,32
	ldw	$9,$29,36
	ldw	$10,$29,40
	ldw	$11,$29,44
	ldw	$12,$29,48
	ldw	$13,$29,52
	ldw	$14,$29,56
	ldw	$15,$29,60
	ldw	$16,$29,64
	ldw	$17,$29,68
	ldw	$18,$29,72
	ldw	$19,$29,76
	ldw	$20,$29,80
	ldw	$21,$29,84
	ldw	$22,$29,88
	ldw	$23,$29,92
	ldw	$24,$29,96
	ldw	$25,$29,100
	ldw	$26,$29,104
	ldw	$27,$29,108
	ldw	$28,$29,112
	ldw	$30,$29,116
	ldw	$31,$29,120
	add	$29,$29,124

	rfx

miss:
	mvfs $4,0
	add $5,$30,$0
	ldw $6,$30,0
	add $7,$29,0
	jal handler_usermiss
	rfx

;***************************************************************

	.code
	.align	4
	
start1:
	; clear PSW
	mvts	$0,0

	; let vector point to RAM
	mvfs	$8,0
	or	$8,$8,1 << 27
	mvts	$8,0
	
	; stack
	add	$29,$0,stack	; set sp
	
	; initialize TLB
	mvts	$0,TLB_ENTRY_LO		; invalidate all TLB entries
	add	$8,$0,dmapaddr		; by impossible virtual page number
	mvts	$8,TLB_ENTRY_HI
	add	$8,$0,$0
	add	$9,$0,TLB_ENTRIES
tlbloop:
	mvts	$8,TLB_INDEX
	tbwi
	add	$8,$8,1
	bne	$8,$9,tlbloop

	; copy data
	add	$10,$0,_bdata		; lowest dst addr to be written to
	add	$8,$0,_edata		; one above the top dst addr
	sub	$9,$8,$10		; $9 = size of data segment
	add	$9,$9,_ecode		; data is waiting right after code
	j	cpytest
cpyloop:
	ldw	$11,$9,0		; src addr in $9
	stw	$11,$8,0		; dst addr in $8
cpytest:
	sub	$8,$8,4			; downward
	sub	$9,$9,4
	bgeu	$8,$10,cpyloop

	; clear bss segment
	add	$8,$0,_bbss		; start with first word of bss
	add	$9,$0,_ebss		; this is one above the top
	j	clrtest
clrloop:
	stw	$0,$8,0			; dst addr in $8
	add	$8,$8,4			; upward
clrtest:
	bltu	$8,$9,clrloop

	; run program
	jal	main		; call 'main'
end_loop:
	j	end_loop	; loop

	.bss

	.align	4
	.space	0x10000
stack:
