/*        ---------------------------------------------------------- 
         |    TITLE:  Keil C51 v7.00 port of Adam Dunkels' uIP v0.9 |
         | REVISION:  VER 0.0                                       |
         | REV.DATE:  30-01-05                                      |
         |  ARCHIVE:                                                |
         |   AUTHOR:  Murray R. Van Luyn, 2005.                     |
          ----------------------------------------------------------         */

/*   --------------------------------------------------------------------- 

    |  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS  | 
    |  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED  | 
    |  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE | 
    |  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY    | 
    |  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL | 
    |  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  | 
    |  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS      | 
    |  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,       | 
    |  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING          | 
    |  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS | 
    |  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.       | 
     ---------------------------------------------------------------------   */

/*=============================================================================
 * Module Name: etherdev.c
 * Purpose: support functions that up layer protocol drivers can call.
 * Author:
 * Date:
 * Notes:
 * $Log$                                                                                                                        
*=============================================================================
*/

/* INCLUDE FILE DECLARATIONS */
/*
#include "intrins.h"
*/
#include "etherdev.h"
#include "uip.h"
#include "timer.h"

/* STATIC VARIABLE DECLARATIONS */
static unsigned int *IoBase = (unsigned int *)0xF0600000;
static UCHAR CurBufRcv = 0;
static UCHAR RcvNextPacket = 0;
static UCHAR XmitPage = 0;
static UCHAR RCV = 0;
static UCHAR DEF_MAC_ADDR[ETH_ADDR_LEN] = { UIP_ETHADDR0,
	UIP_ETHADDR1,
	UIP_ETHADDR2,
	UIP_ETHADDR3,
	UIP_ETHADDR4,
	UIP_ETHADDR5
};

/* LOCAL SUBPROGRAM DECLARATIONS */
static void delay(UINT);
static USHORT etherdev_read(void);

/*
* -----------------------------------------------------------------------------
 * Function Name: delay
 * Purpose: delay function
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
static void delay(UINT loop)
{
	volatile UINT i;

	for (i = 0; i < loop; i++) ;
}	/* End of delay

/*
 * -----------------------------------------------------------------------------
 * Function Name: etherdev_init
 * Purpose: ax88796 initialize procedure.
 * Params:
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void etherdev_init(void)
{
	UCHAR i;

	// reset ax88796 chip
	*(IoBase + NIC_RESET) = 0xff;
	delay(3000);

	// stop MAC interrupt
	*(IoBase + NIC_INTR_MASK) = 0;

	// stop MAC, Page1
	*(IoBase + NIC_COMMAND) = (CR_STOP | CR_NO_DMA | CR_PAGE1);
	delay(1000);

	CurBufRcv = RCV_START_PAGE;

	//Write out the current receive buffer to receive into
	*(IoBase + NIC_CURRENT) = CurBufRcv;

	for (i = 0; i < ETH_ADDR_LEN; i++) {
		*(IoBase + NIC_PHYS_ADDR + i) = DEF_MAC_ADDR[i];
	}

	// stop MAC, Page0
	*(IoBase + NIC_COMMAND) = (CR_STOP | CR_NO_DMA | CR_PAGE0);
	delay(1000);

	*(IoBase + NIC_BOUNDARY) = RCV_STOP_PAGE - 1;

	*(IoBase + NIC_PAGE_START) = RCV_START_PAGE;
	*(IoBase + NIC_PAGE_STOP) = RCV_STOP_PAGE;
	*(IoBase + NIC_DATA_CONFIG) = DCR_BYTE_WIDE | 0x80;
	*(IoBase + NIC_RCV_CONFIG) = RCV_CONFIG;
	*(IoBase + NIC_XMIT_CONFIG) = 0x80;
	*(IoBase + NIC_INTR_STATUS) = 0xff;

	// move back to page 0 and start the card...
	*(IoBase + NIC_COMMAND) = (CR_START | CR_NO_DMA | CR_PAGE0);
	delay(1000);

	CurBufRcv = *(IoBase + 6);
	RcvNextPacket = CurBufRcv;

	uip_len = 20;
	etherdev_send();

}				/* End of etherdev_init */

/*
 * ----------------------------------------------------------------------------
 * Function Name: etherdev_send
 * Purpose: Copy Data to Mac ram and send to Ethernet.
 * Params: none
 * Returns: none
 * Note:
 * ----------------------------------------------------------------------------
 */
void etherdev_send(void)
{
	UCHAR *pBuf = uip_buf;
	USHORT length = uip_len;
	UCHAR bufstart;
	USHORT i;
  
  printf("s1");

	if (XmitPage == 0) {
		bufstart = XMIT_START_PAGE;
		XmitPage = 1;
	} else {
		bufstart = XMIT_START_PAGE + 6;
		XmitPage = 0;

	} /* End of if (XmitPage == 0) */
  printf("2");

	*(IoBase + NIC_RMT_ADDR_LSB) = 0;
	*(IoBase + NIC_RMT_ADDR_MSB) = bufstart;
	*(IoBase + NIC_RMT_COUNT_LSB) = (UCHAR) length;
	*(IoBase + NIC_RMT_COUNT_MSB) = (UCHAR) (length >> 8);

  printf("3");
  
	// set direction (write)
	*(IoBase + NIC_COMMAND) = (CR_START | CR_PAGE0 | CR_DMA_WRITE);

  printf("4");

	i = 40 + UIP_LLH_LEN;
	if (length > i) {
    printf("data\n");
		while (i--) {
      printf("%X (%c) ", *pBuf, *pBuf);
			*(IoBase + NIC_RACK_NIC) = *pBuf++;
		}

		i = length - (40 + UIP_LLH_LEN);
    printf("\nappdata\n", i, uip_appdata);
		while (i--) {
      printf("%X (%c) ", *uip_appdata, *uip_appdata);
			*(IoBase + NIC_RACK_NIC) = *uip_appdata++;
		}
	} else {
    printf("data2\n");
		while (i--) {
      printf("%X (%c) ", *pBuf, *pBuf);
			*(IoBase + NIC_RACK_NIC) = *pBuf++;
    }
	} /* End of if (length > i) */
  
	*(IoBase + NIC_XMIT_START) = bufstart;

	if (length < 60)
		length = 60;

  printf("len %d %d %d\n", length, (UCHAR) length, (UCHAR) (length >> 8));
  
	*(IoBase + NIC_XMIT_COUNT_LSB) = (UCHAR) length;
	*(IoBase + NIC_XMIT_COUNT_MSB) = (UCHAR) (length >> 8);
	*(IoBase + NIC_COMMAND) = (CR_START | CR_XMIT | CR_NO_DMA);

  printf("send end\n");
  
} /* End of etherdev_send */

/*
 * ----------------------------------------------------------------------------
 * Function Name: etherdev_receive
 * Purpose: Copy Data from Mac ram
 * Params:	none
 * Returns: length of current received packet
 * Note:
 * ----------------------------------------------------------------------------
 */
static USHORT etherdev_read(void)
{
	UCHAR *point = uip_buf;
	UCHAR rcvheader[4];
	UCHAR i, interrupt_status;
	USHORT packetlength, index;

	interrupt_status = *(IoBase + NIC_INTR_STATUS);
	if (interrupt_status) {
		*(IoBase + NIC_INTR_STATUS) = interrupt_status;
		if (interrupt_status & (IMR_RCV | IMR_OVERFLOW))
			RCV = 1;
	}

	if (RCV) {
		CurBufRcv = *(IoBase + 6);
		if (CurBufRcv == RcvNextPacket) {
			RCV = 0;
			return 0;
		}

		*(IoBase + NIC_RMT_ADDR_LSB) = 0;
		*(IoBase + NIC_RMT_ADDR_MSB) = RcvNextPacket;
		*(IoBase + NIC_RMT_COUNT_LSB) = 4;
		*(IoBase + NIC_RMT_COUNT_MSB) = 0;

		// set direction (read)
		*(IoBase + NIC_COMMAND) = (CR_START | CR_PAGE0 | CR_DMA_READ);
		for (i = 0; i < 4; i++)
			rcvheader[i] = *(IoBase + NIC_RACK_NIC);

		packetlength = rcvheader[2] + (rcvheader[3] * 256) - 4;

		if ((packetlength > MAX_ETHERNET_LEN)
		    || (packetlength < MIN_ETHERNET_LEN)) {
			RcvNextPacket = rcvheader[1];
			if (RcvNextPacket == RCV_START_PAGE)
				*(IoBase + NIC_BOUNDARY) = RCV_STOP_PAGE - 1;
			else
				*(IoBase + NIC_BOUNDARY) = RcvNextPacket - 1;

			return 0;
		}

		*(IoBase + NIC_RMT_ADDR_LSB) = 4;
		*(IoBase + NIC_RMT_ADDR_MSB) = RcvNextPacket;
		*(IoBase + NIC_RMT_COUNT_LSB) = (UCHAR) packetlength;
		*(IoBase + NIC_RMT_COUNT_MSB) = (UCHAR) (packetlength >> 8);

		// set direction (write)
		*(IoBase + NIC_COMMAND) = (CR_START | CR_PAGE0 | CR_DMA_READ);

		for (index = 0; index < packetlength; index++) {
			*point++ = *(IoBase + NIC_RACK_NIC);
		}

		RcvNextPacket = rcvheader[1];
		if (RcvNextPacket == RCV_START_PAGE)
			*(IoBase + NIC_BOUNDARY) = RCV_STOP_PAGE - 1;
		else
			*(IoBase + NIC_BOUNDARY) = RcvNextPacket - 1;

		return packetlength;

	} /* End of if (RCV) */
	else
		return 0;
}

/*   --------------------------------------------------------------------- 
    |                                                                     |
    |                           etherdev_poll()                           |
    |                                                                     |
    | This function will read an entire IP packet into the uip_buf.       |
    | If it must wait for more than 0.5 seconds, it will return with      |
    | the return value 0. Otherwise, when a full packet has been read     |
    | into the uip_buf buffer, the length of the packet is returned.      |
    |                                                                     |
     ---------------------------------------------------------------------   */
unsigned int etherdev_poll(void)
{
	unsigned int bytes_read;

	/* tick_count threshold should be 12 for 0.5 sec bail-out
	   One second (24) worked better for me, but socket recycling
	   is then slower. I set UIP_TIME_WAIT_TIMEOUT 60 in uipopt.h
	   to counter this. Retransmission timing etc. is affected also. */
	while ((!(bytes_read = etherdev_read())) && (timer0_tick() < 12))
		continue;

	timer0_reset();

	return bytes_read;
}

/*
 * ----------------------------------------------------------------------------
 * Function Name: etherdev_chkmedia
 * Purpose: Read register 0x17 to check ethernet is link or not.
			If media link, check duplex mode on register 0x17 and
			set NIC_XMIT_CONFIG register before exit the function.
			If not link, this function will loop to wait media link.
 * Params:	none
 * Returns:
 * Note:
 * ----------------------------------------------------------------------------
 */
void etherdev_chkmedia(void)
{
	UCHAR status;

	while (1) {
		status = *(IoBase + 0x17);
		if (status & 1)
			break;
		else
			delay(20000);
	}

	if (status & 2)
		*(IoBase + NIC_XMIT_CONFIG) = 0xa0;
	else
		*(IoBase + NIC_XMIT_CONFIG) = 0x20;

} /* End of etherdev_chkmedia */

/* End of etherdev.c */
