//
// eth.v -- Ethernet interface
//


`timescale 1ns/10ps
`default_nettype none


module eth_regs(clk, rst,
                stb, we, addr,
                data_in, data_out,
                ack,
                pbus_d_in, pbus_d_out,
                pbus_drive, pbus_a,
                pbus_read_n, pbus_write_n,
                ether_aen_n, ether_bhe_n,
                ether_cs_n, ether_rdy);
    // internal interface signals
    input clk;
    input rst;
    input stb;
    input we;
    input [7:2] addr;
    input [15:0] data_in;
    output [15:0] data_out;
    output ack;
    // external interface signals
    input [15:0] pbus_d_in;
    output [15:0] pbus_d_out;
    output pbus_drive;
    output [4:0] pbus_a;
    output pbus_read_n;
    output pbus_write_n;
    output ether_aen_n;
    output ether_bhe_n;
    output ether_cs_n;
    input ether_rdy;

  reg [2:0] state;
  reg [15:0] reg_data_r;
  reg [15:0] reg_data_w;
  reg [4:0] reg_addr;
  reg reg_read_n;
  reg reg_write_n;
  reg reg_drive;
  reg reg_aen_n;
  reg reg_bhe_n;
  reg reg_cs_n;
  reg reg_ack;

  always @(posedge clk) begin
    if (rst == 1) begin
      state       <= 2'd0;
      reg_data_r  <= 16'bxxxxxxxxxxxxxxxx;
      reg_data_w  <= 16'bxxxxxxxxxxxxxxxx;
      reg_addr    <= 5'bxxxxx;
      reg_read_n  <= 1;
      reg_write_n <= 1;
      reg_drive   <= 0;
      reg_aen_n   <= 1;
      reg_bhe_n   <= 1;
      reg_cs_n    <= 1;
      reg_ack     <= 0;
    end else begin
      case (state)
        'd0: // Initial: no requests
             // Stay in state 0 until stb is high
          begin
            if (stb) begin
              // Start a device request
              // Do not enable read/write yet
              state       <= 'd1;
              reg_data_r  <= 16'bxxxxxxxxxxxxxxxx;
              reg_data_w  <= data_in[15:0];
              reg_addr    <= addr[6:2];
              reg_read_n  <= 1;
              reg_write_n <= 1;
              reg_drive   <= we;
              reg_aen_n   <= 0;
              reg_bhe_n   <= ~addr[7];
              reg_cs_n    <= 0;
            end
          end
        'd1: // Enable read/write now
          begin
            state         <= 'd2;
            reg_read_n    <= we;
            reg_write_n   <= ~we;
          end
        'd2: // Delay until ether_rdy has a valid value
          begin
            state         <= 'd3;
          end
        'd3:
          begin
            state         <= 'd4;
          end
        'd4: // ether_rdy should have a valid value now
             // stay in this state until ether_rdy is high
          begin
            if (ether_rdy) begin
              // Hold control signals and data
              // but disable read/write.
              // For read requests: read the data now
              state       <= 'd5;
              reg_data_r  <= reg_bhe_n ?
                { 8'b0, pbus_d_in[7:0] } : pbus_d_in[15:0];
              reg_read_n  <= 1;
              reg_write_n <= 1;
              reg_ack     <= 0;
	         end else begin
              state       <= 'd4;
            end
          end
        'd5: // Finish request and send ack to the eco32 bus
          begin
            state         <= 'd6;
            reg_data_r    <= reg_data_r[15:0];
            reg_data_w    <= 16'bxxxxxxxxxxxxxxxx;
            reg_addr      <= 5'bxxxxx;
            reg_read_n    <= 1;
            reg_write_n   <= 1;
            reg_drive     <= 0;
            reg_aen_n     <= 1;
            reg_bhe_n     <= 1;
            reg_cs_n      <= 1;
            reg_ack       <= 1;
          end
        'd6: // Return to state 0 to accept new requests
          begin
            state         <= 'd0;
            reg_ack       <= 0;
          end
      endcase
    end
  end

  assign ack          = reg_ack;
  assign data_out     = reg_data_r[15:0];

  assign pbus_d_out   = reg_data_w[15:0];
  assign pbus_a       = reg_addr[4:0];
  assign pbus_read_n  = reg_read_n;
  assign pbus_write_n = reg_write_n;
  assign pbus_drive   = reg_drive;

  assign ether_aen_n  = reg_aen_n;
  assign ether_bhe_n  = reg_bhe_n;
  assign ether_cs_n   = reg_cs_n;

endmodule
