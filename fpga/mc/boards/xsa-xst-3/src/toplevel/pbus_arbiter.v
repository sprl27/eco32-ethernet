//
// pbus_arbiter.v
//


`timescale 1ns/10ps
`default_nettype none


module pbus_arbiter(clk, rst,
                    pbus_d, pbus_a,
                    pbus_read_n, pbus_write_n,
                    in_0, in_1,
                    drive_0, drive_1,
                    out_0, out_1,
                    a_0, a_1,
                    read_n_0, read_n_1,
                    write_n_0, write_n_1);

    input clk;
    input rst;
    inout [15:0] pbus_d;
    output [4:0] pbus_a;
    output pbus_read_n;
    output pbus_write_n;
    output [15:0] in_0;
    output [15:0] in_1;
    input drive_0;
    input drive_1;
    input [15:0] out_0;
    input [15:0] out_1;
    input [4:0] a_0;
    input [4:0] a_1;
    input read_n_0;
    input read_n_1;
    input write_n_0;
    input write_n_1;

  wire sel;

  assign sel = 1;

  assign pbus_d =
    (sel == 0 & drive_0 == 1)  ? out_0[15:0] :
    (sel == 1 & drive_1 == 1)  ? out_1[15:0] :
    16'bzzzzzzzzzzzzzzzz;

  assign pbus_a =
    (sel == 0)  ? a_0[4:0] :
    (sel == 1)  ? a_1[4:0] :
    5'b00000;

  assign pbus_read_n =
    (sel == 0)  ? read_n_0 :
    (sel == 1)  ? read_n_1 :
    1'b1;

  assign pbus_write_n =
    (sel == 0)  ? write_n_0 :
    (sel == 1)  ? write_n_1 :
    1'b1;

  assign in_0 = pbus_d[15:0];
  assign in_1 = pbus_d[15:0];

endmodule
