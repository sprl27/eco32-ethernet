//
// eth.v -- Ethernet interface
//


`timescale 1ns/10ps
`default_nettype none


module eth(clk, rst,
           stb, we, addr,
           data_in, data_out,
           ack,
           pbus_d_in, pbus_d_out,
           pbus_drive, pbus_a,
           pbus_read_n, pbus_write_n);
    // internal interface signals
    input clk;
    input rst;
    input stb;
    input we;
    input [19:2] addr;
    input [15:0] data_in;
    output [31:0] data_out;
    output ack;
    // external interface signals
    input [15:0] pbus_d_in;
    output [15:0] pbus_d_out;
    output pbus_drive;
    output [4:0] pbus_a;
    output pbus_read_n;
    output pbus_write_n;

  wire regs_stb;
  wire regs_we;
  wire [5:0] regs_addr;
  wire [15:0] regs_data_in;
  wire [15:0] regs_data_out;
  wire regs_ack;

  assign regs_stb = stb && (addr[19:8] == 12'h0);
  assign regs_we = we;
  assign regs_addr = addr[7:2];
  assign regs_data_in = data_in[15:0];

  assign data_out =
    regs_stb ? { 16'h0, regs_data_out[15:0] } : 32'hx;
  assign ack = 
    regs_stb ? regs_ack : 0;

  eth_regs eth_regs_1(
    .clk(clk),
    .rst(rst),
    .stb(regs_stb),
    .we(regs_we),
    .addr(regs_addr[5:0]),
    .data_in(regs_data_in[15:0]),
    .data_out(regs_data_out[15:0]),
    .ack(regs_ack),
    .pbus_d_in(pbus_d_in[15:0]),
    .pbus_d_out(pbus_d_out[15:0]),
    .pbus_drive(pbus_drive),
    .pbus_a(pbus_a[4:0]),
    .pbus_read_n(pbus_read_n),
    .pbus_write_n(pbus_write_n)
  );

endmodule
