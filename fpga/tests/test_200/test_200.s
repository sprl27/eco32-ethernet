;
; ethernet test
;

	.nosyn

; load 8 bit
	ldhi	$4,0xF0600000
	ldw	$5,$4,0

; store 8 bit
	stw	$5,$4,0

; load 16 bit
	ldw	$5,$4,0x80

; store 16 bit
	stw	$5,$4,0x80
	
; bus timeout
	ldw	$5,$4,0x100

; end
	stop:	j stop
